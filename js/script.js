$(document).ready(function() {     
     
    //Execute the slideShow
    slideShow();
	
	//select all the a tag with name equal to modal
    $('a[name=modal]').click(function(e) {
        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = $(this).attr('href');
     
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
     
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
         
        //transition effect    
        //$('#mask').fadeIn(1000);   
        $('#mask').fadeTo("slow",0.8); 
     
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
               
        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);
     
        //transition effect
        $(id).fadeIn(2000);
     
    });
     
    //if close button is clicked
    $('.window .close').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();
        $('#mask, .window').hide();
    });    
     
    //if mask is clicked
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });      
 
});
 
function slideShow() {
 
    //Set the opacity of all images to 0
    $('#gallery .slide').css({opacity: 0.0});
     
    //Get the first image and display it (set it to full opacity)
    $('#gallery .slide:first').css({opacity: 1.0});
	
	$('#gallery .slide:first').addClass('show');
     
    //Call the gallery function to run the slideshow, 6000 = change to next image after 6 seconds
    setInterval('gallery()', 6000);
     
}
 
function gallery() {

    //if no IMGs have the show class, grab the first image
    var current = ( $('#gallery .slide.show') ?  $('#gallery .slide.show') : $('#gallery .slide:first'));
 
    //Get next image,
    var next = ( (current.next().length) 
				? ( current.next() )
				: $('#gallery .slide:first')
				);
     
    //Set the fade in effect for the next image, show class has higher z-index
    next.css({opacity: 0.0})
    .addClass('show')
    .animate({opacity: 1.0}, 1000);
 
    //Hide the current image
    current.animate({opacity: 0.0}, 1000)
    .removeClass('show');
}



